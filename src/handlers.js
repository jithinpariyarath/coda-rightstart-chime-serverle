// Copyright 2019-2020 Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0

const AWS = require('./aws-sdk');
const fs = require('fs');
const uuid = require('./uuid/v4');

// Store meetings in a DynamoDB table so attendees can join by meeting title
const ddb = new AWS.DynamoDB();

// Create an AWS SDK Chime object. Region 'us-east-1' is currently required.
// Use the MediaRegion property below in CreateMeeting to select the region
// the meeting is hosted in.
const chime = new AWS.Chime({ region: 'us-east-1' });

// Set the AWS SDK Chime endpoint. The global endpoint is https://service.chime.aws.amazon.com.
chime.endpoint = new AWS.Endpoint('https://service.chime.aws.amazon.com');

// Read resource names from the environment
const meetingsTableName = process.env.MEETINGS_TABLE_NAME;
const logGroupName = process.env.BROWSER_LOG_GROUP_NAME;
const sqsQueueArn = process.env.SQS_QUEUE_ARN;
const useSqsInsteadOfEventBridge = process.env.USE_EVENT_BRIDGE === 'false';

// === Handlers ===

exports.index = async (event, context, callback) => {
  // Return the contents of the index page
  return response(200, 'text/html', fs.readFileSync('./index.html', {encoding: 'utf8'}));
};

exports.indexV2 = async (event, context, callback) => {
  // Return the contents of the index V2 page
  return response(200, 'text/html', fs.readFileSync('./indexV2.html', {encoding: 'utf8'}));
};

exports.join = async(event, context) => {
  const query = event.queryStringParameters;
  if (!query.title || !query.name || !query.region || !query.password) {
    return response(400, 'application/json', JSON.stringify({error: 'Need parameters: title, name, region, password'}));
  }

  console.log("Querey");
  console.log(query);

  // Look up the meeting by its title. If it does not exist, create the meeting.
  let meeting = await getMeeting(query.title,query.password);
  console.log("Get Meeeting Done");
  console.log(meeting);

  if (!meeting) {
    console.log("Inside fail condition");

    const request = {
      // Use a UUID for the client request token to ensure that any request retries
      // do not create multiple meetings.
      ClientRequestToken: uuid(),

      // Specify the media region (where the meeting is hosted).
      // In this case, we use the region selected by the user.
      MediaRegion: query.region,

      // Set up SQS notifications if being used
      NotificationsConfiguration: useSqsInsteadOfEventBridge ? { SqsQueueArn: sqsQueueArn } : {},

      // Any meeting ID you wish to associate with the meeting.
      // For simplicity here, we use the meeting title.
      ExternalMeetingId: query.title.substring(0, 64),
    };
    console.info('Creating new meeting: ' + JSON.stringify(request));
    meeting = await chime.createMeeting(request).promise();

    console.log("pwd");

    console.log(query.password);

    // Store the meeting in the table using the meeting title as the key.
    await putMeeting(query.title, meeting, query.password);
  }

  // Create new attendee for the meeting
  console.info('Adding new attendee');
  const attendee = (await chime.createAttendee({
    // The meeting ID of the created meeting to add the attendee to
    MeetingId: meeting.Meeting.MeetingId,

    // Any user ID you wish to associate with the attendeee.
    // For simplicity here, we use a random UUID for uniqueness
    // combined with the name the user provided, which can later
    // be used to help build the roster.
    ExternalUserId: `${uuid().substring(0, 8)}#${query.name}`.substring(0, 64),
  }).promise());

  // Return the meeting and attendee responses. The client will use these
  // to join the meeting.
  return response(200, 'application/json', JSON.stringify({
    JoinInfo: {
      Meeting: meeting,
      Attendee: attendee,
    },
  }, null, 2));
};

exports.end = async (event, context) => {
  console.log("Ending meeting");
  console.log(event.queryStringParameters);

  // Fetch the meeting by title
  const meeting = await getMeeting(
    event.queryStringParameters.title,
    event.queryStringParameters.password
  );

  // End the meeting. All attendee connections will hang up.
  await chime.deleteMeeting({ MeetingId: meeting.Meeting.MeetingId }).promise();
  return response(200, 'application/json', JSON.stringify({}));
};

exports.logs = async (event, context) => {
  const body = JSON.parse(event.body);
  if (!body.logs || !body.meetingId || !body.attendeeId || !body.appName) {
    return response(400, 'application/json', JSON.stringify({error: 'Need properties: logs, meetingId, attendeeId, appName'}));
  }
  const logStreamName = `ChimeSDKMeeting_${body.meetingId.toString()}`;
  const cloudWatchClient = new AWS.CloudWatchLogs({ apiVersion: '2014-03-28' });
  const putLogEventsInput = {
    logGroupName: logGroupName,
    logStreamName: logStreamName
  };
  const uploadSequence = await ensureLogStream(cloudWatchClient, logStreamName);
  if (uploadSequence) {
    putLogEventsInput.sequenceToken = uploadSequence;
  }
  const logEvents = [];
  if (body.logs.length !== 0) {
    return response(200, 'application/json', JSON.stringify({}));
  }
  for (let i = 0; i < body.logs.length; i++) {
    const log = body.logs[i];
    const timestamp = new Date(log.timestampMs).toISOString();
    const message = `${timestamp} [${log.sequenceNumber}] [${log.logLevel}] [meeting: ${body.meetingId.toString()}] [attendee: ${body.attendeeId}]: ${log.message}`;
    logEvents.push({
      message: message,
      timestamp: log.timestampMs
    });
  }
  putLogEventsInput.logEvents = logEvents;
  await cloudWatchClient.putLogEvents(putLogEventsInput).promise();
  return response(200, 'application/json', JSON.stringify({}));
};

// Called when SQS receives records of meeting events and logs out those records
exports.sqs_handler = async (event, context, callback) => {
  console.log(event.Records);
  return {};
}

// Called when EventBridge receives a meeting event and logs out the event
exports.event_bridge_handler = async (event, context, callback) => {
  console.log(event);
  return {};
}

// === Helpers ===

// Retrieves the meeting from the table by the meeting title
async function getMeeting(title,password) {

  console.log("Getting meeting with");
  console.log(title);
  console.log(password);

  let params = {
    TableName: meetingsTableName,
    KeyConditionExpression: '#Title = :title',
    ExpressionAttributeValues: {
      ':title': { "S" : title },
      ':password': { "S" : password},
    },
    ExpressionAttributeNames: {
      '#Title': 'Title',
      '#Password': 'Password',
    },
    ScanIndexForward: true,
    Limit: 500,
    FilterExpression: '#Password = :password',
  };


  const result = await ddb.query(params).promise();
  console.log("Outtput result")
  console.log(result);

  try{
    console.log("Innsid etry");
    console.log(JSON.stringify(result.Items[0].Data.S));

    return result.Items && result.Items.length ? JSON.parse(result.Items[0].Data.S) : null;
  }
  catch(err){
    console.log("error occured on fetching title")
    console.log(err);
    return null;
  }
  // return result.Item ? JSON.parse(result.Item.Data.S) : null;
}

// Stores the meeting in the table using the meeting title as the key
async function putMeeting(title, meeting, password) {
  try{
    console.log("Params");
    console.log(title);
    console.log(meeting);
    console.log(password);

  await ddb.putItem({
    TableName: meetingsTableName,
    Item: {
      'Title': { S: title },
      'Password': { S: password },
      'Data': { S: JSON.stringify(meeting) },
      'TTL': {
        N: `${Math.floor(Date.now() / 1000) + 60 * 60 * 24}` // clean up meeting record one day from now
      }
    }
  }).promise();
}catch(err){
  console.log("Errror on put item ")
  console.log(err)
}
}

// Creates log stream if necessary and returns the current sequence token
async function ensureLogStream(cloudWatchClient, logStreamName) {
  const logStreamsResult = await cloudWatchClient.describeLogStreams({
    logGroupName: logGroupName,
    logStreamNamePrefix: logStreamName,
  }).promise();
  const foundStream = logStreamsResult.logStreams.find(s => s.logStreamName === logStreamName);
  if (foundStream) {
    return foundStream.uploadSequenceToken;
  }
  await cloudWatchClient.createLogStream({
    logGroupName: logGroupName,
    logStreamName: logStreamName,
  }).promise();
  return null;
}

function response(statusCode, contentType, body) {
  return {
    statusCode: statusCode,
    headers: { 'Content-Type': contentType , 'Access-Control-Allow-Origin': '*' },
    body: body,
    isBase64Encoded: false
  };
}
